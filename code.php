<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02: A1</title>
	</head>
	<body>
        <h2>Divisible By Five </h2>
		<p><?php printDivisibleByFive(); ?></p>
        <h2>Array methods</h2>
        <?php array_push($students, "John Smith"); ?>
        <pre><?php print_r($students); ?></pre>
        <pre><?php echo count($students)?></pre>
        <?php array_push($students, "Jane Doe"); ?>
        <pre><?php print_r($students); ?></pre>
        <pre><?php echo count($students)?></pre>
        <?php array_shift($students)?>
		<pre><?php print_r($students); ?></pre>
        <pre><?php echo count($students)?></pre>
	</body>
</html>